<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<t:wrapper>
    <style>
        body{
            background-color: #eee;
        }
    </style>


    <form class="form-login" action="index.jsp" method='post'>
        <h2 class="form-login-heading">Tienda</h2>

        <div class="login-wrap">
            <input type="text" class="form-control" placeholder="User ID" autofocus>
            <br>
            <input type="password" class="form-control" placeholder="Password">
            <button class="btn btn-theme btn-block" href="index.html" type="submit"><i class="fa fa-lock"></i> SIGN IN</button>
            <hr>
        </div>
    </form>
    <!-- js placed at the end of the document so the pages load faster -->
    <script src="${pageContext.request.contextPath}/js/jquery.js"></script>
    <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

    <!--BACKSTRETCH-->
    <!-- You can use an image of whatever size. This script will stretch to fit in any screen size.-->
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.backstretch.min.js"></script>
    <script>
        $.backstretch("assets/img/login-bg.jpg", {speed: 500});
    </script>

</t:wrapper>