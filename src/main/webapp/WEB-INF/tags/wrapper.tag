<%@tag description="General Layout" pageEncoding="UTF-8"%>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<%@attribute name="title" required="true"%>
<%@attribute name="js" fragment="true"%>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="Dashboard">
        <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">    

        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.css" rel="stylesheet">
        <!--external css-->
        <link href="${pageContext.request.contextPath}/font-awesome/css/font-awesome.css" rel="stylesheet" />

        <!-- Custom styles for this template -->
        <link href="${pageContext.request.contextPath}/css/style.css" rel="stylesheet">
        <title>Tienda Rosita - ${title}</title>
    </head>
    <body>

        <section id="container" > 

            <sec:authorize access="isAuthenticated()">
                <jsp:include page="/WEB-INF/tags/header.jsp"/>
            </sec:authorize>


            <div class="container-fluid">
                <div class="row">

                    <sec:authorize access="isAuthenticated()">
                        <jsp:include page="/WEB-INF/tags/sidebar.jsp"/>

                        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
                            <jsp:doBody/>
                        </div>
                    </sec:authorize>


                    <sec:authorize access="isAnonymous()">
                        <jsp:doBody/>
                    </sec:authorize>
                </div>

            </div>
        </section>
        <!-- js placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/jquery-ui-1.9.2.custom.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/jquery.ui.touch-punch.min.js"></script>
        <script class="include" type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.dcjqaccordion.2.7.js"></script>
        <script src="${pageContext.request.contextPath}/js/jquery.scrollTo.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/jquery.nicescroll.js" type="text/javascript"></script>


        <!--common script for all pages-->
        <script src="${pageContext.request.contextPath}/js/common-scripts.js"></script>

        <!--script for this page-->

        <script>
            //custom select box

            $(function () {
                $('select.styled').customSelect();
            });

        </script>

    </body>
</html>