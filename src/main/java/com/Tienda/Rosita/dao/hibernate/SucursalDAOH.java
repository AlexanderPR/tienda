/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.Tienda.Rosita.dao.hibernate;

import com.Tienda.Rosita.dao.SucursalDAO;
import com.Tienda.Rosita.model.Sucursal;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;

/**
 *
 * @author pantojarosales
 */

@Repository
public class SucursalDAOH extends HibernateTemplate implements SucursalDAO{
    
    @Autowired
    public SucursalDAOH(SessionFactory sessionFactory){
        super(sessionFactory);
    }

    @Override
    public Sucursal find(Sucursal t) {
        
        Criteria criteria = this.getSession().createCriteria(Sucursal.class);
        criteria.add(Restrictions.eq("id", t.getId()));
        return (Sucursal) criteria.uniqueResult();
        
    }

    @Override
    public List<Sucursal> all() {
        Criteria criteria = this.getSession().createCriteria(Sucursal.class);
        return criteria.list();
    }

    @Override
    public void saveDAO(Sucursal t) {
        this.save(t);
    }

    @Override
    public void updateDAO(Sucursal t) {
        this.merge(t);
    }

    @Override
    public void deleteDAO(Sucursal t) {
        this.delete(t);
    }
    
}
