/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.Tienda.Rosita.controller.Sucursal;

import com.Tienda.Rosita.dao.SucursalDAO;
import com.Tienda.Rosita.model.Sucursal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

/**
 *
 * @author pantojarosales
 */

@Controller
@Transactional
@RequestMapping("general/sucursal")
public class SucursalController {
    
    @Autowired
    SucursalDAO sucursalDAO;
    
    @RequestMapping(method = RequestMethod.GET)
    public String index(Model model) {

        model.addAttribute("sucursal", sucursalDAO.all());
        model.addAttribute("sucursal", new Sucursal());
        return "general/sucursal";
    }
    
    @RequestMapping("update/{id}")
    public String update(@PathVariable("id") Long id, Model model) {

        model.addAttribute("sucursal", sucursalDAO.all());
        model.addAttribute("sucursal", sucursalDAO.find(new Sucursal(id)));
        return "general/sucursal";
    }

    @RequestMapping("save")
    public String save(Sucursal sucursal) {

        if (sucursal.getId() == null) {
            sucursalDAO.saveDAO(sucursal);
        } else {
            sucursalDAO.updateDAO(sucursal);
        }
        return "redirect:/general/sucursal";
    }

    @RequestMapping("delete/{id}")
    public String delete(@PathVariable("id") Long id) {

        sucursalDAO.deleteDAO(new Sucursal(id));
        
        return "redirect:/general/sucursal";
    }

    @ExceptionHandler(Exception.class)
    public String handleConflict() {
        return "redirect:/general/sucursal";
    }
    
}
