-- MySQL dump 10.13  Distrib 5.5.44, for debian-linux-gnu (i686)
--
-- Host: localhost    Database: tienda1
-- ------------------------------------------------------
-- Server version	5.5.44-0ubuntu0.14.10.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Kardex`
--

DROP TABLE IF EXISTS `Kardex`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Kardex` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_kardex_Salida` bigint(20) NOT NULL,
  `id_detalle_Entrada` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_Kardex_salida_idx` (`id_kardex_Salida`),
  KEY `fk_Kardex_entrada_idx` (`id_detalle_Entrada`),
  CONSTRAINT `fk_Kardex_entrada` FOREIGN KEY (`id_detalle_Entrada`) REFERENCES `Kardex_Detalle_entrada` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Kardex_salida` FOREIGN KEY (`id_kardex_Salida`) REFERENCES `Kardex_Detalle_salida` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Kardex`
--

LOCK TABLES `Kardex` WRITE;
/*!40000 ALTER TABLE `Kardex` DISABLE KEYS */;
/*!40000 ALTER TABLE `Kardex` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Kardex_Detalle_entrada`
--

DROP TABLE IF EXISTS `Kardex_Detalle_entrada`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Kardex_Detalle_entrada` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `fecha` date DEFAULT NULL,
  `cantidad` int(11) DEFAULT NULL,
  `precio_compra` double DEFAULT NULL,
  `idPedido` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_Kardex_pedido_idx` (`idPedido`),
  CONSTRAINT `fk_Kardex_pedido` FOREIGN KEY (`idPedido`) REFERENCES `log_Pedido` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Kardex_Detalle_entrada`
--

LOCK TABLES `Kardex_Detalle_entrada` WRITE;
/*!40000 ALTER TABLE `Kardex_Detalle_entrada` DISABLE KEYS */;
/*!40000 ALTER TABLE `Kardex_Detalle_entrada` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Kardex_Detalle_salida`
--

DROP TABLE IF EXISTS `Kardex_Detalle_salida`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Kardex_Detalle_salida` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `fecha` varchar(45) DEFAULT NULL,
  `cantidad` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Kardex_Detalle_salida`
--

LOCK TABLES `Kardex_Detalle_salida` WRITE;
/*!40000 ALTER TABLE `Kardex_Detalle_salida` DISABLE KEYS */;
/*!40000 ALTER TABLE `Kardex_Detalle_salida` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `alm_UnidadMedida`
--

DROP TABLE IF EXISTS `alm_UnidadMedida`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alm_UnidadMedida` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(45) DEFAULT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  `abreviatura` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alm_UnidadMedida`
--

LOCK TABLES `alm_UnidadMedida` WRITE;
/*!40000 ALTER TABLE `alm_UnidadMedida` DISABLE KEYS */;
/*!40000 ALTER TABLE `alm_UnidadMedida` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `alm_categoria`
--

DROP TABLE IF EXISTS `alm_categoria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alm_categoria` (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `cat_nombre` varchar(100) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alm_categoria`
--

LOCK TABLES `alm_categoria` WRITE;
/*!40000 ALTER TABLE `alm_categoria` DISABLE KEYS */;
/*!40000 ALTER TABLE `alm_categoria` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `alm_lote`
--

DROP TABLE IF EXISTS `alm_lote`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alm_lote` (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `prop_costo_unitario` double(20,2) NOT NULL,
  `pro_cantidad_stock` int(50) NOT NULL,
  `pro_fecha_Vencimiento` date DEFAULT NULL,
  `lote` varchar(45) DEFAULT NULL,
  `fecha_registro` datetime DEFAULT NULL,
  `estado` varchar(45) NOT NULL,
  `Id_Marca` bigint(20) NOT NULL,
  `Id_Categoria` bigint(20) NOT NULL,
  `id_Sucursal` bigint(20) NOT NULL,
  `id_unidadMedida` bigint(20) NOT NULL,
  `id_kardex` bigint(20) DEFAULT NULL,
  `id_producto` bigint(20) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `IdCategoria` (`Id_Categoria`),
  KEY `IdMarca` (`Id_Marca`),
  KEY `IdSucursal` (`id_Sucursal`),
  KEY `idProducto` (`id_producto`),
  KEY `fk_UnidadMedida_idx` (`id_unidadMedida`),
  KEY `fk_Kardex_idx` (`id_kardex`),
  CONSTRAINT `fk_categoria_clave` FOREIGN KEY (`Id_Categoria`) REFERENCES `alm_categoria` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Kardex_clave` FOREIGN KEY (`id_kardex`) REFERENCES `Kardex` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_marca_clave` FOREIGN KEY (`Id_Marca`) REFERENCES `alm_marca` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_producto_clave` FOREIGN KEY (`id_producto`) REFERENCES `alm_producto` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_sucursal_clave` FOREIGN KEY (`id_Sucursal`) REFERENCES `sucursal` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_UnidadMedida_clave` FOREIGN KEY (`id_unidadMedida`) REFERENCES `alm_UnidadMedida` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alm_lote`
--

LOCK TABLES `alm_lote` WRITE;
/*!40000 ALTER TABLE `alm_lote` DISABLE KEYS */;
/*!40000 ALTER TABLE `alm_lote` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `alm_marca`
--

DROP TABLE IF EXISTS `alm_marca`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alm_marca` (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alm_marca`
--

LOCK TABLES `alm_marca` WRITE;
/*!40000 ALTER TABLE `alm_marca` DISABLE KEYS */;
/*!40000 ALTER TABLE `alm_marca` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `alm_producto`
--

DROP TABLE IF EXISTS `alm_producto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alm_producto` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) DEFAULT NULL,
  `descripcion` varchar(45) DEFAULT NULL,
  `minimo` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alm_producto`
--

LOCK TABLES `alm_producto` WRITE;
/*!40000 ALTER TABLE `alm_producto` DISABLE KEYS */;
/*!40000 ALTER TABLE `alm_producto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `com_venta`
--

DROP TABLE IF EXISTS `com_venta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `com_venta` (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `cliente` varchar(50) NOT NULL,
  `fecha_venta` datetime DEFAULT NULL,
  `total_venta` double(20,2) NOT NULL,
  `estado_venta` int(30) NOT NULL,
  `comprobante` int(50) NOT NULL,
  `estado` varchar(45) DEFAULT NULL,
  `id_Empleado` bigint(20) NOT NULL,
  `id_cliente` bigint(20) NOT NULL,
  `id_venta_producto` bigint(20) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `DniRucCliente` (`id_cliente`),
  KEY `DniEmpleado` (`id_Empleado`),
  KEY `fk_venta_producto_idx` (`id_venta_producto`),
  CONSTRAINT `fk_cliente` FOREIGN KEY (`id_cliente`) REFERENCES `gen_contacto` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_empleado_clave` FOREIGN KEY (`id_Empleado`) REFERENCES `gen_empleado` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_venta_producto` FOREIGN KEY (`id_venta_producto`) REFERENCES `com_ventaItem` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `com_venta`
--

LOCK TABLES `com_venta` WRITE;
/*!40000 ALTER TABLE `com_venta` DISABLE KEYS */;
/*!40000 ALTER TABLE `com_venta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `com_ventaItem`
--

DROP TABLE IF EXISTS `com_ventaItem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `com_ventaItem` (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `cantidad` int(50) NOT NULL,
  `ven_pro_costo_total` double(20,2) NOT NULL,
  `id_producto` bigint(20) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `IdVenta` (`Id`),
  KEY `IdProducto` (`id_producto`),
  CONSTRAINT `fk_ventaproducto_1` FOREIGN KEY (`id_producto`) REFERENCES `alm_lote` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `com_ventaItem`
--

LOCK TABLES `com_ventaItem` WRITE;
/*!40000 ALTER TABLE `com_ventaItem` DISABLE KEYS */;
/*!40000 ALTER TABLE `com_ventaItem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gen_TipoDocumento`
--

DROP TABLE IF EXISTS `gen_TipoDocumento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gen_TipoDocumento` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `tipo_descripcion` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gen_TipoDocumento`
--

LOCK TABLES `gen_TipoDocumento` WRITE;
/*!40000 ALTER TABLE `gen_TipoDocumento` DISABLE KEYS */;
/*!40000 ALTER TABLE `gen_TipoDocumento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gen_contacto`
--

DROP TABLE IF EXISTS `gen_contacto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gen_contacto` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(45) DEFAULT NULL,
  `descuento` double DEFAULT NULL,
  `contacto` varchar(500) DEFAULT NULL,
  `id_persona` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_cliente_1_idx` (`id_persona`),
  CONSTRAINT `fk_cliente_1` FOREIGN KEY (`id_persona`) REFERENCES `gen_persona` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gen_contacto`
--

LOCK TABLES `gen_contacto` WRITE;
/*!40000 ALTER TABLE `gen_contacto` DISABLE KEYS */;
/*!40000 ALTER TABLE `gen_contacto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gen_empleado`
--

DROP TABLE IF EXISTS `gen_empleado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gen_empleado` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `email` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `emp_imagen` varchar(100) NOT NULL,
  `Id_rol` bigint(20) NOT NULL,
  `Id_sucursal` bigint(20) NOT NULL,
  `Id_persona` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IdSucursal` (`Id_sucursal`),
  KEY `IdRol` (`Id_rol`),
  KEY `fk_persona_idx` (`Id_persona`),
  CONSTRAINT `fk_persona` FOREIGN KEY (`Id_persona`) REFERENCES `gen_persona` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_rol` FOREIGN KEY (`Id_rol`) REFERENCES `sec_rol` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_sucursal` FOREIGN KEY (`Id_sucursal`) REFERENCES `sucursal` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gen_empleado`
--

LOCK TABLES `gen_empleado` WRITE;
/*!40000 ALTER TABLE `gen_empleado` DISABLE KEYS */;
/*!40000 ALTER TABLE `gen_empleado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gen_persona`
--

DROP TABLE IF EXISTS `gen_persona`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gen_persona` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  `apellido` varchar(50) NOT NULL,
  `telefono` varchar(50) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `direccion` varchar(45) DEFAULT NULL,
  `numero_doc` varchar(45) DEFAULT NULL,
  `id_tipo_documento` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tipo_documento_idx` (`id_tipo_documento`),
  CONSTRAINT `fk_tipo_documento` FOREIGN KEY (`id_tipo_documento`) REFERENCES `gen_TipoDocumento` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gen_persona`
--

LOCK TABLES `gen_persona` WRITE;
/*!40000 ALTER TABLE `gen_persona` DISABLE KEYS */;
/*!40000 ALTER TABLE `gen_persona` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `log_Pedido`
--

DROP TABLE IF EXISTS `log_Pedido`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log_Pedido` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `fechadeIngreso` datetime DEFAULT NULL,
  `TotalOrden` double(20,2) NOT NULL,
  `piado` tinyint(1) DEFAULT NULL,
  `fechadePago` date DEFAULT NULL,
  `total_restante` double(20,2) NOT NULL,
  `comprobante` int(50) NOT NULL,
  `id_empleado` bigint(20) NOT NULL,
  `id_proveedor` bigint(20) NOT NULL,
  `estado` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `DniRucProveedor` (`id_empleado`),
  KEY `fk_Pedido_contacto1_idx` (`id_proveedor`),
  CONSTRAINT `fk_empleado` FOREIGN KEY (`id_empleado`) REFERENCES `gen_empleado` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Pedido_contacto1` FOREIGN KEY (`id_proveedor`) REFERENCES `gen_contacto` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `log_Pedido`
--

LOCK TABLES `log_Pedido` WRITE;
/*!40000 ALTER TABLE `log_Pedido` DISABLE KEYS */;
/*!40000 ALTER TABLE `log_Pedido` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sec_rol`
--

DROP TABLE IF EXISTS `sec_rol`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sec_rol` (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `rol_nombre` varchar(45) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sec_rol`
--

LOCK TABLES `sec_rol` WRITE;
/*!40000 ALTER TABLE `sec_rol` DISABLE KEYS */;
/*!40000 ALTER TABLE `sec_rol` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sucursal`
--

DROP TABLE IF EXISTS `sucursal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sucursal` (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  `direccion` varchar(200) NOT NULL,
  `telefono` varchar(50) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sucursal`
--

LOCK TABLES `sucursal` WRITE;
/*!40000 ALTER TABLE `sucursal` DISABLE KEYS */;
/*!40000 ALTER TABLE `sucursal` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-08-05  5:21:36
